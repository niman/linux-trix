from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'views.home'),
    url(r'^about/', 'views.about'),
    url(r'^page/', include('page.urls')),
    url(r'^contactus/', include('contactus.urls')),

    url(r'^forum/', include('forum.urls')),
    url(r'^ultra_blog/', include('ultra_blog.urls')),
    url(r'^news/', include('news.urls')),
    url(r'^auth/', include('auth.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
if settings.DEBUG:
    urlpatterns += patterns('',
                (r'^statics/(?P<path>.*)$',
                'django.views.static.serve',
                 {'document_root': settings.MEDIA_ROOT}),
            )
