from django.shortcuts import render_to_response as rr
from django.template import RequestContext


def home(request):
    return rr("index.html",
              {},
              context_instance=RequestContext(request))


def about(request):
    return rr("about.html",
              {},
              context_instance=RequestContext(request))
